import math
import random

import pygame
import pygame as p

# select if run on arcade or laptop
on_arcade = False
keycolors = {11: "black-button", 13: "blue-button", 15: "yellow-button", 29: "green-button"}
if on_arcade:
    import RPi.GPIO as GPIO
    import time

    GPIO.setmode(GPIO.BOARD)
    buttonPins = [11, 13, 15, 29]
    for pin in buttonPins:
        GPIO.setup(pin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

## Flip-n-Fire - Arcade 2D Spaceshooter
## Achtung Hobbyprojekt!
## Gerne Verbesserungen in Softwarequalität und Codelesbarkeit Pushen
## Natürlich auch gerne Fragen.

## Gameengine = pygame

p.init()
fps = 25
# colors
space = [0, 0, 10]
retrogreen = [50, 200, 50]
red = [250, 5, 5]
white = [253, 253, 253]
blue = [50, 50, 250]
yellow = [255, 255, 5]

# global variables
score = 0
gameheat = 0
asteroidnum = 0
aliennum = 0
current_level = 0
gameheatByCoin = 20
gameheatByExplosion = 50
initial_health = 4
clock = p.time.Clock()
screen = p.display.set_mode([0, 0], pygame.NOFRAME)  # 0,0 = fullscreen
screen.fill(space)
screen_x, screen_y = screen.get_size()
field_x = screen_x  # play field upper 80% of the screen
field_y = 0.8 * screen_y
keylist = []

# fonts
font_mega = p.font.Font("assets/font_Beefd.ttf", 40)
font_title = p.font.Font("assets/font_Beefd.ttf", 25)
font_small = p.font.Font("assets/font_Beefd.ttf", 18)
font_tiny = p.font.Font("assets/font_Beefd.ttf", 12)

effekt_volume = 0.5
musik_volume = 1
snd_ship_laser = pygame.mixer.Sound("assets/snd_ship_laser.wav")
snd_ship_laser.set_volume(effekt_volume)
snd_warp = pygame.mixer.Sound("assets/warp.wav")
snd_warp.set_volume(effekt_volume)
snd_pickup = pygame.mixer.Sound("assets/snd_pickup.wav")
snd_pickup.set_volume(effekt_volume)
snd_heal = pygame.mixer.Sound("assets/snd_heal.wav")
snd_heal.set_volume(1)
snd_ship_hit = pygame.mixer.Sound("assets/snd_ship_hit.wav")
snd_ship_hit.set_volume(effekt_volume)
snd_explosion = pygame.mixer.Sound("assets/snd_explosion.wav")
snd_explosion.set_volume(effekt_volume)
snd_gameover = pygame.mixer.Sound("assets/snd_gameover.wav")
snd_gameover.set_volume(0.8 * musik_volume)
song_intro = pygame.mixer.Sound("assets/intro.mp3")
song_intro.set_volume(musik_volume)
song_game = pygame.mixer.Sound("assets/funk.mp3")
song_game.set_volume(musik_volume)
song_game2 = pygame.mixer.Sound("assets/song2.mp3")
song_game2.set_volume(musik_volume)
song_gameover = pygame.mixer.Sound("assets/pwned.mp3")
song_gameover.set_volume(musik_volume)

# screen setup
p.display.set_caption("FlipnFire")
img = p.image.load("assets/img_FabCade_logo.png").convert()
p.display.set_icon(img)

ship_group = pygame.sprite.Group()
bullet_group = pygame.sprite.Group()
alien_group = pygame.sprite.Group()
coin_group = pygame.sprite.Group()
star_group = pygame.sprite.Group()
explosion_group = pygame.sprite.Group()
asteroid_group = pygame.sprite.Group()
heart_group = pygame.sprite.Group()
portal_group = pygame.sprite.Group()

objective_num = 0
objective_name = ""

run = True

# level-array
## [asteroidnum, aliennum, objectivenum, objectivename

Level_objectives = [[3, 0, 4, "Coin"],
                    [7, 0, 12, "Asteroid"],
                    [10, 1, 4, "Alien"],
                    [3, 4, 6, "Asteroid"],
                    [10, 5, 10, "Coin"],
                    [10, 4, 5, "Alien"]]

current_level = 0

keys_hold = []


def keys_pressed():
    keys = []
    if on_arcade:
        for e in p.event.get():
            if e.type == p.KEYDOWN:
                if e.key == p.K_ESCAPE:
                    quit()
        for pin in buttonPins:
            if GPIO.input(pin):
                keys.append(keycolors[pin])
    else:
        for e in p.event.get():
            if e.type == p.KEYDOWN:
                if e.key == p.K_ESCAPE:
                    return False
                keys.append(p.key.name(e.key))
    return keys


# calculate position change by speed and angle (radians)
def move(speed, angle):
    new_x = (speed * math.cos(angle))
    new_y = (speed * math.sin(angle))
    return new_x, new_y


# return player color according to player number. return color name if name=true, return color rgb if name=false
def num2color(number, name):
    if name:
        color = "blue"
        if number == 2:
            color = "yellow"
        if number == 3:
            color = "red"
        if number == 4:
            color = "green"
        return color
    color = blue
    if number == 2:
        color = yellow
    if number == 3:
        color = red
    if number == 4:
        color = retrogreen
    return color


class Ship(pygame.sprite.Sprite):  # uses pygame Sprite class by creating a child in it
    def __init__(self, x, y, angle, health, number, key):
        self.color = num2color(number, False)
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("assets/img_ship_" + num2color(number, True) + ".png").convert()  # load image
        scale = (field_x / 50) / self.image.get_size()[0]  # shipsize 2% of field (in x dimension)
        self.image = pygame.transform.scale(self.image,
                                            (self.image.get_size()[0] * scale, self.image.get_size()[1] * scale))
        degree = (angle * 180 / math.pi) * -1 - 90
        self.image = pygame.transform.rotate(self.image, degree)
        self.base_image = self.image  # base image for constant rotation
        self.rect = self.image.get_rect()  # convert image to rectangle object
        self.rect.center = [x, y]  # center of rect at x,y
        self.health_start = health
        self.health_remaining = health
        self.ship_speed = 8
        self.cooldown = 7
        self.dead = False
        self.angle = angle  # current angle in which the ship is facing
        self.spin = 0.06  # current spin acceleration
        self.maxspin = 0.25  # max spin built up
        self.minspin = 0.05
        self.direction = 1  # spin direction
        self.spin_changerate = 0.002
        self.score = 0
        self.player_number = number
        self.spawn = self.rect.center
        self.spawning = True
        self.spawncooldown = 20
        self.cooldown_curr = self.spawncooldown
        self.mybutton = key
        self.porting = False

    def update(self):
        if self.dead:
            return
        if self.porting:  # be sucked into the middle
            self.angle += self.spin
            distance = (field_x / 2 - self.rect.centerx) * 0.05, (field_y / 2 - self.rect.centery) * 0.05
            self.rect = self.rect.move(distance)
            degree = (self.angle * 180 / math.pi) * -1
            self.image = pygame.transform.rotate(self.base_image, degree)
            return
        if self.spawning:
            self.cooldown_curr -= 1
            if self.cooldown_curr == 0:
                self.cooldown_curr = self.cooldown
                self.spawning = False
            return
        # movement
        if self.cooldown_curr > 0:  # cooldown for flipping
            self.cooldown_curr -= 1

        self.angle += self.spin
        if (self.spin < self.maxspin or self.direction < 0) and (self.spin > self.maxspin * -1 or self.direction > 0):
            self.spin += self.spin_changerate * self.direction

        if keys_hold.__contains__(self.mybutton) and self.cooldown_curr == 0:
            self.direction *= -1
            self.spin = self.minspin * self.direction
            self.cooldown_curr = self.cooldown
            snd_ship_laser.play()
            bullet = Ship_Bullets(self.rect.centerx, self.rect.centery, 2 * self.ship_speed, self.angle,
                                  self.player_number)
            bullet_group.add(bullet)
        self.rect = self.rect.move(move(self.ship_speed, self.angle))
        degree = (self.angle * 180 / math.pi) * -1
        self.image = pygame.transform.rotate(self.base_image, degree)
        old_center = self.rect.center
        self.rect = self.image.get_rect()
        self.rect.center = old_center
        # border collision
        if self.rect.left < 0 or self.rect.top < 0 or self.rect.right > field_x or self.rect.bottom > field_y:
            self.loselife()

        # asteroid collide
        if pygame.sprite.spritecollide(self, asteroid_group, True):
            self.loselife()

        # alien collide
        if pygame.sprite.spritecollide(self, alien_group, False):
            self.loselife()

        # coin collision
        if pygame.sprite.spritecollide(self, coin_group, True):
            coin = Coin()
            coin_group.add(coin)
            snd_pickup.play()
            self.score += 100
            global objective_num
            if (objective_num > 0 and objective_name == "Coin"):
                objective_num -= 1
        # heart collision
        if pygame.sprite.spritecollide(self, heart_group, True):
            snd_heal.play()
            self.score += 200
            for ship in ship_group:
                ship.addlife()
        # portal collision
        if pygame.sprite.spritecollide(self, portal_group, False):
            self.porting = True
        return

    def loselife(self):
        self.health_remaining -= 1
        explosion = Explosion(self.rect.center)
        explosion_group.add(explosion)
        if self.health_remaining == 0:
            self.dead = True
            snd_gameover.play()
            return False
        snd_explosion.play()
        self.rect.center = self.spawn
        self.spawning = True
        self.cooldown_curr = self.spawncooldown
        return

    def addlife(self):
        self.health_remaining += 1
        if self.dead:
            self.dead = False
            self.rect.center = self.spawn
            self.spawning = True
            self.cooldown_curr = self.spawncooldown


class Coin(pygame.sprite.Sprite):
    def __init__(self):
        global gameheat
        gameheat += gameheatByCoin
        pygame.sprite.Sprite.__init__(self)
        self.anim_images = []  # list of images for the animation
        for i_img in range(0, 3):  # the 2nd argument is exclusive
            image = pygame.image.load(f"assets/coin{i_img}.png").convert()
            scale = (field_x / 50) / image.get_size()[0]  # coinsize 2% of field (in x dimension)
            image = pygame.transform.scale(image, (image.get_size()[0] * scale, image.get_size()[1] * scale))
            self.anim_images.append(image)  # add images to animation list
        self.anim_index = random.randint(0, 2)  # pointer to animation list
        self.image = self.anim_images[self.anim_index]
        self.rect = self.image.get_rect()
        self.rect.center = (field_x * (random.random() * 0.8 + 0.1), field_y * (random.random() * 0.8 + 0.1))
        self.anim_speed = 15
        self.counter = 0

    def update(self):
        self.counter += 1
        if self.counter > self.anim_speed:
            self.counter = 0
            self.anim_index += 1
            if self.anim_index > 2:
                self.anim_index = 0
            self.image = self.anim_images[self.anim_index]


class Heart(pygame.sprite.Sprite):
    def __init__(self, center):
        pygame.sprite.Sprite.__init__(self)
        self.anim_images = []  # list of images for the animation
        for i_img in range(0, 3):  # the 2nd argument is exclusive
            image = pygame.image.load(f"assets/herz{i_img}.png").convert()
            scale = (field_x / 50) / image.get_size()[0]  # coinsize 2% of field (in x dimension)
            image = pygame.transform.scale(image, (image.get_size()[0] * scale, image.get_size()[1] * scale))
            self.anim_images.append(image)  # add images to animation list
        self.anim_index = random.randint(0, 2)  # pointer to animation list
        self.image = self.anim_images[self.anim_index]
        self.rect = self.image.get_rect()
        self.rect.center = center
        self.anim_speed = 15
        self.counter = 0

    def update(self):
        self.counter += 1
        if self.counter > self.anim_speed:
            self.counter = 0
            self.anim_index += 1
            if self.anim_index > 2:
                self.anim_index = 0
            self.image = self.anim_images[self.anim_index]


class BackgroundStar(pygame.sprite.Sprite):  # uses pygame Sprite class by creating a child in it
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.anim_images = []  # list of images for the animation
        randsize = random.random() * 0.8 + 0.8  # variations in star sizes
        for i_img in range(0, 6):  # the 2nd argument is exclusive
            image = pygame.image.load(f"assets/star{i_img}.png").convert()
            scale = (field_x / 150) / image.get_size()[0] * randsize  # coinsize 0.66% of field (in x dimension)
            image = pygame.transform.scale(image, (image.get_size()[0] * scale, image.get_size()[1] * scale))
            self.anim_images.append(image)  # add images to animation list
        self.anim_index = random.randint(0, 3)  # pointer to animation list
        self.image = self.anim_images[self.anim_index]
        self.rect = self.image.get_rect()  # convert image to rectangle object
        self.rect.center = self.rect.center = (field_x * random.random(), field_y * random.random())
        self.anim_speed = 15
        self.counter = random.randint(0, self.anim_speed)  # for control of animations speed

    def update(self):
        self.counter += 1
        if self.counter > self.anim_speed:
            self.counter = 0
            self.anim_index += 1
            if self.anim_index > 3:
                self.anim_index = 0
            self.image = self.anim_images[self.anim_index]


class Ship_Bullets(pygame.sprite.Sprite):

    def __init__(self, x, y, speed, angle, playernum):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("assets/img_bullet_" + num2color(playernum, True) + ".png").convert()
        scale = (field_x / 200) / self.image.get_size()[0]  # bulletsize 0.5% of field (in x dimension)
        self.image = pygame.transform.scale(self.image,
                                            (self.image.get_size()[0] * scale, self.image.get_size()[1] * scale))
        degree = (angle * 180 / math.pi) * -1 - 90
        self.image = pygame.transform.rotate(self.image, degree)
        self.rect = self.image.get_rect()
        self.rect.center = [x, y]
        self.x = x
        self.y = y
        self.angle = angle
        self.speed = speed
        self.playernum = playernum

    def update(self):
        self.rect = self.rect.move(move(self.speed, self.angle))
        if self.rect.left < 0 or self.rect.top < 0 or self.rect.right > field_x or self.rect.bottom > field_y:
            self.kill()
        if pygame.sprite.spritecollide(self, asteroid_group, True):
            explosion = Explosion(self.rect.center)
            explosion_group.add(explosion)
            snd_ship_hit.play()
            shiplist = ship_group.sprites()
            shiplist[self.playernum - 1].score += 500
            global objective_num
            if (objective_num > 0 and objective_name == "Asteroid"):
                objective_num -= 1

            # random heart every 10th asteroid
            if random.random() > 0.9:
                heart = Heart(self.rect.center)
                heart_group.add(heart)
            self.kill()


class Explosion(pygame.sprite.Sprite):  # uses pygame Sprite class by creating a child in it
    def __init__(self, center):
        global gameheat
        gameheat += gameheatByExplosion
        pygame.sprite.Sprite.__init__(self)
        self.anim_images = []  # list of images for the animation
        for i_img in range(1, 8):  # the 2nd argument is exclusive
            image = pygame.image.load(f"assets/img_Explosion_{i_img}.png").convert()
            scale = (field_x / 20) / image.get_size()[0]  # bulletsize 5% of field (in x dimension)
            image = pygame.transform.scale(image, (image.get_size()[0] * scale, image.get_size()[1] * scale))
            self.anim_images.append(image)  # add images to animation list
        self.anim_index = 0  # pointer to animation list
        self.image = self.anim_images[self.anim_index]
        self.rect = self.image.get_rect()  # convert image to rectangle object
        self.rect.center = center  # center of rect at x,y
        self.counter = 0  # for control of animations speed

    def update(self):
        explosion_speed = 3
        self.counter += 1
        if self.counter > explosion_speed and self.anim_index < len(self.anim_images) - 1:
            self.counter = 0
            self.anim_index += 1
            self.image = self.anim_images[self.anim_index]
        # anims end
        if self.counter > len(self.anim_images) and self.counter >= explosion_speed:
            self.kill()


class Portal(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.anim_images = []  # list of images for the animation
        for i_img in range(2, 43):  # the 2nd argument is exclusive
            image = pygame.image.load(f"assets/Portal/img_Portal_ ({i_img}).png").convert()
            scale = (field_x / 10) / image.get_size()[0]  # portalsize 5% of field (in x dimension)
            image = pygame.transform.scale(image, (image.get_size()[0] * scale, image.get_size()[1] * scale))
            self.anim_images.append(image)  # add images to animation list
        self.anim_index = 0  # pointer to animation list
        self.image = self.anim_images[self.anim_index]
        self.rect = self.image.get_rect()
        self.rect.center = [field_x * 0.5, field_y * 0.5]
        self.anim_speed = 1
        self.counter = 0

    def grow(self):
        scale = 1.2
        self.image = pygame.transform.scale(self.image,
                                            (self.image.get_size()[0] * scale, self.image.get_size()[1] * scale))
        self.rect = self.image.get_rect()
        self.rect.center = [field_x * 0.5, field_y * 0.5]

    def update(self):
        self.counter += 1
        if self.counter > self.anim_speed:
            self.counter = 0
            self.anim_index += 1
            if self.anim_index > len(self.anim_images) - 1:
                self.anim_index = 0
            self.image = self.anim_images[self.anim_index]
            self.mask = pygame.mask.from_surface(self.image).scale([0.2, 0.2])


class Asteroid(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        side = random.randint(0, 3)  # asteroid from random site: top, right, bottom, left
        if side == 0:
            x = random.randint(0, field_x)
            y = 0
            angle = random.random() * math.pi  # 0- pi (downwards)
        elif side == 1:
            x = field_x
            y = random.randint(0, field_y)
            angle = random.random() * math.pi + 1.25 * math.pi  # 0.75pi-1.25pi (leftwards)
        elif side == 2:
            x = random.randint(0, field_x)
            y = field_y
            angle = random.random() * math.pi + math.pi  # pi-1.5 pi (upwards)
        else:
            x = 0
            y = random.randint(0, field_y)
            angle = random.random() * math.pi + 0.25 * math.pi  # 0.25pi-1.25pi (rightwards)

        self.image = pygame.image.load("assets/img_Asteroid.png").convert()
        scale = (field_x / 50) / self.image.get_size()[0]  # bulletsize 2% of field (in x dimension)
        self.image = pygame.transform.scale(self.image,
                                            (self.image.get_size()[0] * scale, self.image.get_size()[1] * scale))
        self.rect = self.image.get_rect()
        self.rect.center = [x, y]
        self.speed = 5
        self.angle = angle

    def update(self):
        self.rect = self.rect.move(move(self.speed, self.angle))

        if self.rect.centerx < 0 or self.rect.centery < 0 or self.rect.centerx > field_x or self.rect.centery > field_y:
            self.kill()
        # update a mask to ignore transparent pixels in collisions
        self.mask = pygame.mask.from_surface(self.image)


class Alien(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        side = random.randint(0, 3)  # asteroid from random site: top, right, bottom, left
        if side == 0:
            x = random.randint(0, field_x)
            y = 0
            angle = random.random() * math.pi  # 0- pi (downwards)
        elif side == 1:
            x = field_x
            y = random.randint(0, field_y)
            angle = random.random() * math.pi + 1.25 * math.pi  # 0.75pi-1.25pi (leftwards)
        elif side == 2:
            x = random.randint(0, field_x)
            y = field_y
            angle = random.random() * math.pi + math.pi  # pi-1.5 pi (upwards)
        else:
            x = 0
            y = random.randint(0, field_y)
            angle = random.random() * math.pi + 0.25 * math.pi  # 0.25pi-1.25pi (rightwards)

        self.image = pygame.image.load("assets/img_alienbig_1.png").convert()
        self.anim_images = []  # list of images for the animation
        for i_img in range(1, 5):  # the 2nd argument is exclusive
            image = pygame.image.load(f"assets/img_alienbig_{i_img}.png").convert()
            scale = (field_x / 20) / image.get_size()[0]  # coinsize 5% of field (in x dimension)
            image = pygame.transform.scale(image, (image.get_size()[0] * scale, image.get_size()[1] * scale))
            degree = (angle * 180 / math.pi) * -1 + 90
            image = pygame.transform.rotate(image, degree)

            self.anim_images.append(image)  # add images to animation list
        self.anim_index = len(self.anim_images) - 1  # pointer to animation list
        self.image = self.anim_images[self.anim_index]
        self.rect = self.image.get_rect()
        self.rect.center = [x, y]
        self.speed = 2
        self.angle = angle
        self.max_health = 6
        self.current_health = self.max_health

    def update(self):
        self.rect = self.rect.move(move(self.speed, self.angle))

        if pygame.sprite.spritecollide(self, bullet_group, True):
            self.current_health -= 1
            snd_ship_hit.play()
            health_percentage = self.current_health / self.max_health
            self.anim_index = int(len(self.anim_images) * health_percentage)
            self.image = self.anim_images[self.anim_index]
            # anims end

            if self.current_health == 0:
                global objective_num
                if (objective_num > 0 and objective_name == "Alien"):
                    objective_num -= 1
                explosion = Explosion(self.rect.center)
                explosion_group.add(explosion)
                shiplist = ship_group.sprites()
                for ship in shiplist:
                    ship.score += 500
                self.kill()

        if self.rect.centerx < 0 or self.rect.centery < 0 or self.rect.centerx > field_x or self.rect.centery > field_y:
            self.kill()
        # update a mask to ignore transparent pixels in collisions
        self.mask = pygame.mask.from_surface(self.image)


def startscreen():
    song_intro.stop()
    song_intro.play()
    song_gameover.stop()
    screen.fill(space)
    text1 = font_mega.render("Flip'n'Fire!", True, red)
    text2 = font_small.render("Press your button to join the game", True, retrogreen)
    text3 = font_small.render("Press again to start.", True, retrogreen)
    screen.blit(text1, (screen_x * 0.25, screen_y * 0.25))
    screen.blit(text2, (screen_x * 0.1, screen_y * 0.45))
    screen.blit(text3, (screen_x * 0.1, screen_y * 0.50))
    pygame.display.flip()
    wait = 3  # number of button hits till wait is over
    global keylist
    keylist = list()
    while wait > 0:
        clock.tick(fps)
        pressed_keys = keys_pressed()
        for key in pressed_keys:
            if key not in keylist:
                if len(keylist) < 4:
                    keylist.append(key)
                    text = font_small.render(
                        "Player " + len(keylist).__str__() + " joined. Your Button: " + key, True,
                        num2color(len(keylist), False))
                    screen.blit(text, (screen_x * 0.1, screen_y * 0.6 + 35 * len(keylist)))
                    pygame.display.flip()
            else:
                wait = wait - 1  # start if same button pressed twice

        clock.tick(fps)
    return keylist


def draw_field():
    bottom = pygame.draw.line(screen, retrogreen, (0, field_y), (field_x, field_y), 5)
    top = pygame.draw.line(screen, retrogreen, (0, 0), (field_x, 0), 5)
    left = pygame.draw.line(screen, retrogreen, (0, 0), (0, field_y), 5)
    right = pygame.draw.line(screen, retrogreen, (field_x, 0), (field_x, field_y), 5)


def plotinfo():
    s = ""
    if objective_num > 1:
        s = "s"
    text3 = font_tiny.render(
        "Level " + (current_level + 1).__str__() + ": Objective: " + objective_num.__str__() + " " + objective_name + s,
        True, retrogreen)
    if len(portal_group) > 0:
        text3 = font_tiny.render("Portal is open! Jump to next level!", True, retrogreen)
    screen.blit(text3, (screen_x * 0.3, screen_y * 0.82))
    for ship in ship_group:
        status = ""
        if ship.dead:
            status = "dead"
        else:
            if ship.porting:
                status = "porting"
            else:
                if ship.spawning: status = "spawning"
        text = font_tiny.render(status, True, ship.color)
        text0 = font_tiny.render("Player: " + ship.player_number.__str__(), True, ship.color)
        text1 = font_tiny.render("Score: " + ship.score.__str__(), True, ship.color)
        text2 = font_tiny.render("Lives: " + ship.health_remaining.__str__(), True, ship.color)
        xoffset = (ship.player_number - 1) * screen_x * 0.25
        screen.blit(text, (10 + xoffset, screen_y * 0.85))
        screen.blit(text0, (10 + xoffset, screen_y * 0.88))
        screen.blit(text1, (10 + xoffset, screen_y * 0.91))
        screen.blit(text2, (10 + xoffset, screen_y * 0.94))
    pass


def gamecompleted():
    song_game.stop()
    song_game2.play(-1)
    screen.fill(space)
    headline = font_mega.render("Game completed!", True, retrogreen)
    subline = font_tiny.render("You have successfully saved the Universe. You may keep the coins!", True, white)
    text = font_small.render("Final scores: ", True, retrogreen)
    screen.blit(headline, (0.1 * screen_x, screen_y * 0.15))
    screen.blit(subline, (0.1 * screen_x, screen_y * 0.25))
    screen.blit(text, (0.3 * screen_x, screen_y * 0.35))
    p.display.update()
    for ship in ship_group:
        for i in range(fps): clock.tick(fps)  # every second
        text = font_small.render("Player " + ship.player_number.__str__() + ": " + ship.score.__str__(), True,
                                 num2color(ship.player_number, False))
        yoffset = (ship.player_number - 1) * screen_y * 0.1
        screen.blit(text, (0.3 * screen_x, screen_y * 0.4 + yoffset))
        p.display.update()
    text = font_small.render("Press button to restart. ", True, retrogreen)
    screen.blit(text, (0.4 * screen_x, screen_y * 0.9))
    p.display.update()
    wait = True
    while wait:
        # exit on escape
        clock.tick(fps)
        if keys_pressed(): return True
    pass



def init_level():
    keys_hold = []
    warping = False
    gameover = False
    global keylist, gameheat
    gameheat = 0

    if current_level == 0:
        keylist = startscreen()
    song_intro.stop()

    if current_level > 2:
        song_game.play(-1)
    else:
        song_game2.play(-1)
    ship_group.empty()
    alien_group.empty()
    asteroid_group.empty()
    heart_group.empty()
    portal_group.empty()
    i = 1
    for key in keylist:
        ship = Ship(int(field_x / 5) * i, field_y * 0.3, 0, initial_health, i,
                    key)  # x, y, angle, health; player number
        ship_group.add(ship)
        i += 1
    coin_group.empty()
    coin = Coin()
    coin_group.add(coin)
    explosion_group.empty()
    star_group.empty()
    portal_group.empty()
    bullet_group.empty()
    for i in range(50):
        star = BackgroundStar()
        star_group.add(star)
    global objective_num, objective_name, aliennum, objective_num
    objective_num = Level_objectives[current_level][2]
    global objective_name
    objective_name = Level_objectives[current_level][3]
    asteroidnum = Level_objectives[current_level][0]
    aliennum = Level_objectives[current_level][1]

    for i in range(asteroidnum):
        asteroid = Asteroid()
        asteroid_group.add(asteroid)

    for i in range(aliennum):
        alien = Alien()
        alien_group.add(alien)
    return True


run = init_level()


def draw_everything():
    screen.fill(space)
    draw_field()

    star_group.update()
    star_group.draw(screen)

    portal_group.update()
    portal_group.draw(screen)

    asteroid_group.update()
    asteroid_group.draw(screen)

    coin_group.update()
    coin_group.draw(screen)

    bullet_group.update()
    bullet_group.draw(screen)

    heart_group.update()
    heart_group.draw(screen)

    explosion_group.update()
    explosion_group.draw(screen)

    alien_group.update()
    alien_group.draw(screen)

    ship_group.update()
    ship_group.draw(screen)

    plotinfo()

    if objective_num == 0 and portal_group.__len__() == 0:
        portal = Portal()
        portal_group.add(portal)

    pygame.display.update()
    pass


def gameoverscreen():
    song_game.stop()
    song_gameover.play()
    image = pygame.image.load(f"assets/img_Gameover.png").convert()
    rect = image.get_rect()
    rect.center = [0.5 * field_x, 0.5 * field_y]
    screen.blit(image, rect)
    for i in range(5 * fps):  # wait 5 secs
        explosion_group.update()
        explosion_group.draw(screen)
        pygame.display.update()
        clock.tick(fps)
    screen.fill(space)
    text = font_title.render("Final scores: ", True, retrogreen)
    screen.blit(text, (0.3 * screen_x, screen_y * 0.25))
    p.display.update()
    for ship in ship_group:
        for i in range(fps): clock.tick(fps)  # every second
        text = font_small.render("Player " + ship.player_number.__str__() + ": " + ship.score.__str__(), True,
                                 num2color(ship.player_number, False))
        yoffset = (ship.player_number - 1) * screen_y * 0.1
        screen.blit(text, (0.3 * screen_x, screen_y * 0.4 + yoffset))
        p.display.update()
    text = font_small.render("Press button to restart. ", True, retrogreen)
    screen.blit(text, (0.4 * screen_x, screen_y * 0.9))
    p.display.update()
    wait = True
    while wait:
        # exit on escape
        clock.tick(fps)
        for e in p.event.get():
            if e.type == p.KEYDOWN:
                if e.key == p.K_ESCAPE:
                    quit()
        if keys_pressed(): return True
    pass




def note(textlist, secs):
    pygame.draw.rect(screen, space, pygame.Rect(field_x * 0.1, field_y * 0.1, field_x * 0.8, field_y * 0.8))
    pygame.draw.rect(screen, retrogreen, pygame.Rect(field_x * 0.1, field_y * 0.1, field_x * 0.8, field_y * 0.8), 3)
    pygame.display.flip()
    i = 0
    for text in textlist:
        text1 = font_tiny.render(text, True, retrogreen)
        screen.blit(text1, (field_x * 0.15, field_y * (0.2 + i * 0.1)))
        i += 1
        pygame.display.update()
        for j in range(fps):
            clock.tick(fps)
            for e in p.event.get():
                if e.type == p.KEYDOWN:
                    wait = False
                    return
    wait = True
    if secs > 0:  # if secs are provided, wait those secs
        wait = False
        for j in range(secs * fps):
            clock.tick(fps)
    while wait == True:  # if no secs provided, wait till button
        text1 = font_small.render("Press button to continue", True, retrogreen)
        screen.blit(text1, (field_x * 0.5, field_y * 0.85))
        pygame.display.update()
        if keys_pressed():
            wait = False
        clock.tick(fps)
    screen.fill(space)
    p.display.update()


# gameloop
note(("Commander: The aliens are attacking, you got to stop them!",
      "You: But the cockpit is damaged. We only have one button left!",
      "Commander: Then use it to Flip and Fire! Only you can save the world!", "You: Wait, what?",
      "Commander: Good luck!"), -1)


def warpingscreen():
    song_game.stop()
    ##warp animation
    snd_warp.play()
    for i in range(25):
        for Portal in portal_group:
            Portal.grow()
        portal_group.draw(screen)
        p.display.update()
        clock.tick(fps)
    global current_level
    song_game.stop()
    screen.fill(space)
    text = font_title.render("Level " + (current_level + 1).__str__() + " done! ", True, retrogreen)
    screen.blit(text, (0.3 * screen_x, screen_y * 0.25))
    text = font_small.render("Press button to continue. ", True, retrogreen)
    screen.blit(text, (0.4 * screen_x, screen_y * 0.9))
    p.display.update()
    wait = True
    while wait:
        # exit on escape
        clock.tick(fps)
        if keys_pressed():
            wait = False
    current_level = current_level + 1
    if current_level == len(Level_objectives):  # loop on max level
        gamecompleted()
    return True


while run:
    draw_everything()  # includes updates
    # gameheat
    if len(alien_group) < aliennum + (gameheat / 400) and not len(alien_group) > 5:
        alien = Alien()
        alien_group.add(alien)
    if len(asteroid_group) < asteroidnum + (gameheat / 250) and not len(asteroid_group) > 15:
        asteroid = Asteroid()
        asteroid_group.add(asteroid)

    gameover = True
    for ship in ship_group:
        if not ship.dead:
            gameover = False
    warping = True
    for ship in ship_group:
        if not ship.porting and not ship.dead:  # if single ship is not yet in portal, level is not finished.
            warping = False

    if warping and not gameover:
        warpingscreen()
        warping = False
        init_level()

    if gameover:
        if gameoverscreen():
            gameover = False
            current_level = 0
            run = init_level()
        else:
            run = False

    # buttons!
    if on_arcade:
        for e in p.event.get():
            if e.type == p.KEYDOWN:
                if e.key == p.K_ESCAPE:
                    quit()
        for pin in buttonPins:
            if GPIO.input(pin):
                keys_hold.append(keycolors[pin])
            if not GPIO.input(pin):
                while keys_hold.__contains__(keycolors[pin]):
                    keys_hold.remove(keycolors[pin])
    else:
        for e in p.event.get():
            if e.type == p.KEYDOWN:
                if e.key == p.K_ESCAPE:
                    quit()
                keys_hold.append(p.key.name(e.key))
            if e.type == p.KEYUP:
                while keys_hold.__contains__(p.key.name(e.key)):
                    keys_hold.remove(p.key.name(e.key))
    clock.tick(fps)
p.quit()
